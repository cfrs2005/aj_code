<?php
/**
 * Bubble_sort_method.php
 * ==============================================
 * Copy right 2013-2014 http://www.80aj.com
 * ----------------------------------------------
 * This is not a free software, without any authorization is not allowed to use and spread.
 * ==============================================
 * @param unknowtype
 * @return return_type
 * @author: aj
 * @date: 2014-10-10
 * @version: v1.0.0
 */
// by http://www.oschina.net/code/snippet_724468_39092
/**
 * 冒泡排序算法的运作如下：（从后往前）
 * 比较相邻的元素。如果第一个比第二个大，就交换他们两个。
 * 对每一对相邻元素作同样的工作，从开始第一对到结尾的最后一对。在这一点，最后的元素应该会是最大的数。
 * 针对所有的元素重复以上的步骤，除了最后一个。
 * 持续每次对越来越少的元素重复上面的步骤，直到没有任何一对数字需要比较。
 */
$array = array (
		7,
		1,
		2,
		8,
		4,
		5,
		6,
		0,
		22,
		9 
);
$len = count ( $array );
for($i = 0; $i < $len; $i ++) {
	for($j = $len - 1; $j > $i; $j --) {
		if ($array [$j - 1] > $array [$j]) {
			$tmp = $array [$j];
			$array [$j] = $array [$j - 1];
			$array [$j - 1] = $tmp;
			echo implode ( $array, ',' ) . "\n\r";
		}
	}
}

/** print
7,1,2,8,4,5,6,0,9,22

7,1,2,8,4,5,0,6,9,22

7,1,2,8,4,0,5,6,9,22

7,1,2,8,0,4,5,6,9,22

7,1,2,0,8,4,5,6,9,22

7,1,0,2,8,4,5,6,9,22

7,0,1,2,8,4,5,6,9,22

0,7,1,2,8,4,5,6,9,22

0,7,1,2,4,8,5,6,9,22

0,1,7,2,4,8,5,6,9,22

0,1,7,2,4,5,8,6,9,22

0,1,2,7,4,5,8,6,9,22

0,1,2,7,4,5,6,8,9,22

0,1,2,4,7,5,6,8,9,22

0,1,2,4,5,7,6,8,9,22

0,1,2,4,5,6,7,8,9,22
 */