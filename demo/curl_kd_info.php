<?php
/**
 * curl_kd_info.php
 * ==============================================
 * Copy right 2013-2014 http://www.80aj.com
 * ----------------------------------------------
 * This is not a free software, without any authorization is not allowed to use and spread.
 * ==============================================
 * @param get_kingdomconquest info
 * @return return_type
 * @author: 80aj
 * @date: 2014-3-18
 * @version: v1.0.0
 */
set_time_limit ( 0 );
class curl_multi {
	private $url_list = array ();
	private $curl_setopt = array (
			'CURLOPT_RETURNTRANSFER' => 1,
			'CURLOPT_HEADER' => 0,
			'CURLOPT_NOBODY' => 0,
			'CURLOPT_FOLLOWLOCATION' => 0,
			'CURLOPT_TIMEOUT' => 30 
	);
	function __construct($seconds = 30) {
		set_time_limit ( $seconds );
	}
	/**
	 * 设置网址
	 *
	 * @list 数组
	 */
	public function setUrlList($list = array()) {
		$this->url_list = $list;
	}
	/**
	 * 设置参数
	 *
	 * @cutPot array
	 */
	public function setOpt($cutPot) {
		$this->curl_setopt = $cutPot + $this->curl_setopt;
	}
	/**
	 * 执行
	 *
	 * @return array
	 */
	public function execute() {
		$mh = curl_multi_init ();
		foreach ( $this->url_list as $i => $url ) {
			$conn [$i] = curl_init ( $url );
			foreach ( $this->curl_setopt as $key => $val ) {
				curl_setopt ( $conn [$i], preg_replace ( '/(CURLOPT_\w{1,})/ie', '$0', $key ), $val );
			}
			curl_multi_add_handle ( $mh, $conn [$i] );
		}
		$active = false;
		do {
			$mrc = curl_multi_exec ( $mh, $active );
		} while ( $mrc == CURLM_CALL_MULTI_PERFORM );
		
		while ( $active and $mrc == CURLM_OK ) {
			if (curl_multi_select ( $mh ) != - 1) {
				do {
					$mrc = curl_multi_exec ( $mh, $active );
				} while ( $mrc == CURLM_CALL_MULTI_PERFORM );
			}
		}
		$res = array ();
		foreach ( $this->url_list as $i => $url ) {
			$res [$i] = curl_multi_getcontent ( $conn [$i] );
			curl_close ( $conn [$i] );
			curl_multi_remove_handle ( $mh, $conn [$i] );
		}
		curl_multi_close ( $mh );
		return $res;
	}
}

$curl_mul = new curl_multi ();

$new_arr = array ();
for($i = 120; $i < 300; $i ++) {
	echo "http://kingdom-conquest.jp/en/info/view_campaign.php?id=" . md5 ( $i );
	$new_arr [] = "http://kingdom-conquest.jp/en/info/view_campaign.php?id=" . md5 ( $i );
}

$curl_mul->setUrlList ( $new_arr );
$a = $curl_mul->execute ();
$i = 1;
foreach ( $a as $v ) {
	$filename = $i . '.html';
	$fp2 = @fopen ( $filename, 'a' );
	fwrite ( $fp2, $v );
	fclose ( $fp2 );
	$i ++;
}