<?php
/**
 * switch_new_action.php
 * ==============================================
 * Copy right 2013-2014 http://www.80aj.com
 * ----------------------------------------------
 * This is not a free software, without any authorization is not allowed to use and spread.
 * ==============================================
 * @param unknowtype
 * @return return_type
 * @author: aj
 * @date: 2014-10-10
 * @version: v1.0.0
 */
$val = 10;
$i = 'm';
echo $val . $i . "\n\r";
$get_kbs = function () use($val, $i) {
	switch ($i) {
		case 'g' :
			$val *= 1024;
		case 'm' :
			$val *= 1024;
		case 'k' :
			$val *= 1024;
	}
	return $val;
};
echo $get_kbs ();